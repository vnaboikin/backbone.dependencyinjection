// backbone.dependencyinjection - v0.1.2
// The MIT License
// Copyright (c) 2014 Vadim Naboikin <vadim@naboikin.com>

/*jslint plusplus: true, vars: true, browser: true, passfail: false, devel: true, nomen: true*/
/*global define, require, Backbone, _, exports*/

(function (factory) {
    'use strict';

    if (typeof define === 'function' && define.amd) {  // for AMD
        define(['underscore', 'backbone'], factory);
    } else if (typeof exports === 'object') { // for Node.js or CommonJS
        factory(require('underscore'), require('backbone'));
    } else { // default
        factory(_, Backbone);
    }

}(function (_, Backbone) {
    'use strict';

    var BackboneMixin;

    /**
     *
     * @param constr
     * @returns {constr}
     * @description
     * Inject services on Class instantiation
     */
    function applyInjections (constr) {
        var constrCopy = constr;

        constr = function () {
            var toinject, i;

            /*jslint forin:true */
            for (i in this) {
                toinject = /^inject\:([a-zA-Z0-9\-]+)$/.exec(this[i]);
                if (toinject && toinject[1]) {
                    this[i] = this.inject(toinject[1]);
                }
            }
            /*jslint forin:false */

            constrCopy.apply(this, arguments);
        };

        constr.prototype = constrCopy.prototype;
        constr.extend = constrCopy.extend;
        return constr;
    }

    /**
     *
     * @type {{inject: inject, factory: factory, register: register, dropDependency: dropDependency, isRegistered: isRegistered}}
     */
    BackboneMixin = {
        /**
         *
         * @param name
         * @param [params] optional params for constructor
         * @returns {*}
         * @description
         * Injects registered service
         */
        inject: function (name, params) {
            return Backbone.Injector.inject(name, params);
        },

        /**
         *
         * @param name
         * @param params
         * @return {*}
         * @description
         * returns new instance of the given ('name') class
         */
        factory: function (name, params) {
            return Backbone.Injector.factory(name, params);
        },

        /**
         *
         * @param name
         * @param service
         * @description
         * Register service
         */
        register: function (name, service) {
            Backbone.Injector.register(name, service);
        },

        /**
         *
         * @param name
         * @description
         * Remove service completely
         */
        dropDependency: function (name) {
            Backbone.Injector.dropService(name);
        },

        /**
         *
         * @param name
         * @returns {boolean}
         * @description
         * Checks whether service exists or not
         */
        isRegistered: function (name) {
            return Backbone.Injector.isRegistered(name);
        }
    };

    /**
     *
     * @type {{constructors: {}, instances: {}, isExists: isRegistered, getService: inject, registerInstance: _registerInstance, registerConstructor: _registerConstructor, deleteService: dropDependency, clearCache: clearCache, _clearInstance: _clearInstance}}
     */
    Backbone.Injector = {
        services: {},

        /**
         *
         * @param name
         * @returns {boolean}
         */
        isRegistered: function (name) {
            var exists = false;
            if (this._isServiceExists(name)) {
                exists = this._isInstanceExists(name) ? 'instance' : 'constructors';
            }
            return exists;
        },

        /**
         *
         * @param name
         * @param [params] optional params for constructor
         * @returns {*}
         */
        inject: function (name, params) {
            var exists = this.isRegistered(name),
                service;

            if (!exists) {
                throw "There is no service with the name: " + name;
            }

            switch (exists) {
                case 'instance':
                    service = this.services[name].instance;
                    break;
                case 'constructors':
                    service = new this.services[name].Constructor(params);
                    this._registerInstance(name, service);
                    break;
            }

            this.trigger('inject:' + name, {
                name: name,
                service: service
            });
            return service;
        },

        /**
         *
         * @param {string} name
         * @param {*} [params] - optional params for constructor
         * @returns {*}
         */
        factory: function (name, params) {
            var instance;

            if (!this._isConstructorExists(name)) {
                throw "There is no constructor with the name: " + name;
            }

            params = params || {};
            instance = new this.services[name].Constructor(params);

            this.trigger('factory:' + name, {
                name: name,
                instance: instance
            });

            return instance;
        },

        /**
         *
         * @param name
         * @param service
         */
        register: function (name, service) {
            if (typeof service === 'function') {
                this._registerConstructor(name, service);
            } else {
                this._registerInstance(name, service);
            }

            this.trigger('register', {
                name: name
            });
        },

        /**
         *
         * @param name
         * @description
         * Completely removes service
         */
        dropService: function (name) {
            if (this._isServiceExists(name)) {
                Backbone.Injector.trigger('dropDependency:' + name);

                this._clearConstructor(name);
                this._clearInstance(name);
            }
        },

        /**
         *
         * @param name
         */
        clearCache: function (name) {
            if (this._isConstructorExists(name) && this._isInstanceExists(name)) {
                this._clearInstance(name);
            }
        },

        /**
         *
         * @param name
         * @param instance
         */
        _registerInstance: function (name, instance) {
            this.services[name] = this.services[name] || {};
            if (this._isInstanceExists(name)) {
                throw "Service already exists: " + name;
            }

            this.services[name].instance = instance;
        },

        /**
         *
         * @param name
         * @param classConstructor
         */
        _registerConstructor: function (name, classConstructor) {
            this.services[name] = this.services[name] || {};
            if (typeof classConstructor !== 'function') {
                throw "Wrong Constructor";
            }
            if (this.services[name].Constructor || this.services[name].instance) {
                throw "Service already already exists: " + name;
            }

            this.services[name].Constructor = classConstructor;
        },

        /**
         *
         * @param name
         * @returns {boolean}
         * @private
         */
        _isServiceExists: function (name) {
            return this.services[name] ? true : false;
        },

        /**
         *
         * @param name
         * @returns {boolean}
         * @private
         */
        _isConstructorExists: function (name) {
            return this.services[name].Constructor ? true : false;
        },

        /**
         *
         * @param name
         * @returns {boolean}
         * @private
         */
        _isInstanceExists: function (name) {
            return this.services[name].instance ? true : false;
        },

        /**
         *
         * @param name
         * @private
         */
        _clearConstructor: function (name) {
            if (this._isConstructorExists(name)) {
                this.services[name].Constructor = null;
                delete this.services[name].Constructor;
            }
        },

        /**
         *
         * @param name
         * @private
         * @description
         * Remove service instance
         */
        _clearInstance: function (name) {
            if (this._isInstanceExists(name)) {
                this.services[name].instance = null;
                delete this.services[name].instance;
            }
        }
    };

    //Extend classes;
    _.extend(Backbone.Injector, Backbone.Events);
    _.extend(Backbone.View.prototype, BackboneMixin);
    _.extend(Backbone.Model.prototype, BackboneMixin);
    _.extend(Backbone.Collection.prototype, BackboneMixin);
    _.extend(Backbone.Router.prototype, BackboneMixin);

    Backbone.View = applyInjections(Backbone.View);
    Backbone.Model = applyInjections(Backbone.Model);
    Backbone.Collection = applyInjections(Backbone.Collection);
    Backbone.Router = applyInjections(Backbone.Router);
}));