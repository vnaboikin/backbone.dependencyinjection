/**
 * Created by vadym.naboikin on 8/14/2014.
 */

/*jslint plusplus: true, vars: true, browser: true, passfail: false, devel: true, nomen: true*/
/*global module, ok, test, Backbone, equal, notEqual, expect, sinon, throws*/

(function () {
    'use strict';
    module('Backbone Dependency Injection private methods tests', {
        setup: function () {
            var testClass = 'class',
                testInstance = 'instance',
                TestConstructor = function (params) {
                    var i;
                    for (i in params) {
                        if (params.hasOwnProperty(i)) {
                            this[i] = params[i];
                        }
                    }
                };
            Backbone.Injector.services[testClass] = {};
            Backbone.Injector.services[testClass].Constructor = TestConstructor;

            Backbone.Injector.services[testInstance] = {};
            Backbone.Injector.services[testInstance].instance = new TestConstructor({
                test: 'test'
            });
        },

        teardown: function () {
            var testClass = 'class',
                testInstance = 'instance';
            Backbone.Injector.services[testClass] = {};
            Backbone.Injector.services[testInstance] = {};
            delete Backbone.Injector.services[testClass];
            delete Backbone.Injector.services[testInstance];
            Backbone.Injector.stopListening();
        }
    });

    test('_registerInstance method test', function () {
        var isInstanceExists = this.stub(Backbone.Injector, '_isInstanceExists'),
            name = 'instance',
            instance = {
                test: 'test'
            };

        expect(2);
        isInstanceExists.returns(false);
        Backbone.Injector._registerInstance('instance', instance);
        equal(Backbone.Injector.services[name].instance, instance, "Instance is regestered");

        isInstanceExists.returns(true);
        throws(function () {
            Backbone.Injector._registerInstance('instance', instance);
        }, /Service already exists:/, 'Throws exception: "Service already exists: ' + name + '"');
        isInstanceExists.restore();
    });

    test('_registerConstructor method test', function () {
        var constructor = function () {
                this.test = 'test';
            },
            wrongConstructor = {
                test: 'test'
            },
            name = 'privateConstructor';

        expect(4);
        throws(function () {
            Backbone.Injector._registerConstructor(name, wrongConstructor);
        }, /Wrong Constructor/, 'Throws exception: "Wrong Constructor"');

        throws(function () {
            Backbone.Injector._registerConstructor('class', constructor);
        }, /Service already already exists:/, 'Throws exception: "Service already already exists: class"');
        throws(function () {
            Backbone.Injector._registerConstructor('instance', constructor);
        }, /Service already already exists:/, 'Throws exception: "Service already already exists: instance"');

        Backbone.Injector._registerConstructor(name, constructor);
        equal(Backbone.Injector.services[name].Constructor, constructor, "Constructor is registered");

        Backbone.Injector.services[name] = {};
        delete Backbone.Injector.services[name];
    });

    test('_isServiceExists method test', function () {
        var registered;

        expect(2);
        registered = Backbone.Injector._isServiceExists('instance');
        ok(registered, "Service is registered");
        registered = Backbone.Injector._isServiceExists('notregistered');
        ok(!registered, "Service is not registered");
    });

    test('_isConstructorExists method test', function () {
        var testClass = 'class',
            testInstance = 'instance',
            registered;

        expect(2);
        registered = Backbone.Injector._isConstructorExists(testClass);
        ok(registered, "Constructor is registered");
        registered = Backbone.Injector._isConstructorExists(testInstance);
        ok(!registered, "Constructor is not registered");
    });

    test('_isInstanceExists method test', function () {
        var testClass = 'class',
            testInstance = 'instance',
            registered;

        expect(2);
        registered = Backbone.Injector._isInstanceExists(testInstance);
        ok(registered, "Instance is registered");
        registered = Backbone.Injector._isInstanceExists(testClass);
        ok(!registered, "Instance is not registered");
    });

    test('_clearConstructor method test', function () {
        var isConstructorExists = this.stub(Backbone.Injector, '_isConstructorExists'),
            testClass = 'class';

        expect(2);
        isConstructorExists.returns(false);
        Backbone.Injector._clearConstructor(testClass);
        notEqual(Backbone.Injector.services[testClass].Constructor, undefined, "Constructor didn't removed");

        isConstructorExists.returns(true);
        Backbone.Injector._clearConstructor(testClass);
        equal(Backbone.Injector.services[testClass].Constructor, undefined, "Constructor removed");
        isConstructorExists.restore();
    });

    test('_clearInstance method test', function () {
        var testInstance = 'instance',
            isInstanceExists = this.stub(Backbone.Injector, '_isInstanceExists');

        expect(2);
        isInstanceExists.returns(false);
        Backbone.Injector._clearInstance(testInstance);
        notEqual(Backbone.Injector.services[testInstance].instance, undefined, "Instance didn't removed");

        isInstanceExists.returns(true);
        Backbone.Injector._clearInstance(testInstance);
        equal(Backbone.Injector.services[testInstance].instance, undefined, "Instance removed");
    });
}());