/**
 * Created by vadym.naboikin on 8/4/2014.
 */
/*jslint plusplus: true, vars: true, browser: true, passfail: false, devel: true, nomen: true*/
/*global module, ok, test, Backbone, equal, notEqual, expect, sinon, throws*/

(function () {
    'use strict';
    module('Backbone Dependency Injection public methods tests', {
        setup: function () {
            var testClass = 'class',
                testInstance = 'instance',
                TestConstructor = function (params) {
                    var i;
                    for (i in params) {
                        if (params.hasOwnProperty(i)) {
                            this[i] = params[i];
                        }
                    }
                };
            Backbone.Injector.services[testClass] = {};
            Backbone.Injector.services[testClass].Constructor = TestConstructor;

            Backbone.Injector.services[testInstance] = {};
            Backbone.Injector.services[testInstance].instance = new TestConstructor({
                test: 'test'
            });
        },

        teardown: function () {
            var testClass = 'class',
                testInstance = 'instance';

            Backbone.Injector.services[testClass] = {};
            Backbone.Injector.services[testInstance] = {};
            delete Backbone.Injector.services[testClass];
            delete Backbone.Injector.services[testInstance];
            Backbone.Injector.stopListening();
        }
    });

    // isRegistered method test
    test('isRegistered method test', function () {
        var isServiceExists = this.stub(Backbone.Injector, '_isServiceExists'),
            isInstanceExists = this.stub(Backbone.Injector, '_isInstanceExists');

        expect(3);

        isServiceExists.returns(false);
        ok(!Backbone.Injector.isRegistered('name'), "Service doesn't exista");

        isServiceExists.returns(true);
        isInstanceExists.returns(true);
        equal(Backbone.Injector.isRegistered('name'), 'instance', "Returns 'instance' if instance exists");

        isInstanceExists.returns(false);
        equal(Backbone.Injector.isRegistered('name'), 'constructors', "Returns 'constructors' if there is no instance available and constructor exists");

        isServiceExists.restore();
        isInstanceExists.restore();
    });

    // inject method test
    test('inject method test', function () {
        var isRegistered = this.stub(Backbone.Injector, 'isRegistered'),
            testClass = 'class',
            testInstance = 'instance',
            injectInstance,
            injectClass,
            injectFromCache,
            classInjectEvent = this.spy(),
            instanceInjectEvent = this.spy();


        expect(13);

        isRegistered.returns(false);
        throws(function () {
            Backbone.Injector.inject('test');
        }, /There is no service with the name:/, 'Throws exception: There is no constructor with the name: "test"');

        isRegistered.returns('instance');
        Backbone.Injector.listenTo(Backbone.Injector, 'inject:' + testClass, classInjectEvent);
        Backbone.Injector.listenTo(Backbone.Injector, 'inject:' + testInstance, instanceInjectEvent);


        injectInstance = Backbone.Injector.inject(testInstance);
        equal(injectInstance.test, 'test', 'Inject instance');
        ok(instanceInjectEvent.calledOnce, 'Inject event triggered once');
        equal(instanceInjectEvent.args[0][0].name, testInstance, 'Inject event pass right arguments');
        equal(instanceInjectEvent.args[0][0].service, injectInstance, 'Inject event pass right arguments');

        isRegistered.returns('constructors');
        injectClass = Backbone.Injector.inject(testClass, {
            anotherTest: 'anotherTest'
        });
        equal(injectClass.anotherTest, 'anotherTest', 'Inject class constructor');
        equal(classInjectEvent.args[0][0].name, testClass, 'Inject event pass right arguments');
        equal(classInjectEvent.args[0][0].service, injectClass, 'Inject event pass right arguments');

        isRegistered.returns('instance');
        injectFromCache = Backbone.Injector.inject(testClass, {
            anotherTest: 'test'
        });
        equal(injectFromCache, injectClass, 'Injecting from cache');
        notEqual(injectFromCache.anotherTest, 'test', 'Injecting from cache - new params are ignored');
        ok(classInjectEvent.calledTwice, 'Inject class constructor called twice');
        equal(classInjectEvent.args[0][0].service, injectFromCache, 'Inject event pass right arguments');
        equal(classInjectEvent.args[0][0].service, classInjectEvent.args[1][0].service, 'Inject event pass right arguments');

        isRegistered.restore();
    });

    //factory method test
    test('factory method test', function () {
        var isConstructorExists = this.stub(Backbone.Injector, '_isConstructorExists'),
            registerEvent = this.spy(),
            serviceName = 'class',
            testParams = {
                test: 'test'
            },
            firstResult,
            secondResult;

        Backbone.Injector.listenTo(Backbone.Injector, 'factory:' + serviceName, registerEvent);
        isConstructorExists.returns(false);
        throws(function () {
            Backbone.Injector.factory('test');
        }, /There is no constructor with the name:/, 'Throws exception: There is no constructor with the name: "test"');
        ok(isConstructorExists.calledOnce, '_isConstructorExists called once');

        isConstructorExists.returns(true);
        firstResult = Backbone.Injector.factory(serviceName, testParams);
        equal(firstResult.test, 'test', "New class instance has been created");
        equal(registerEvent.args[0][0].name, serviceName, 'factory:' + serviceName + " event pass right arguments");
        equal(registerEvent.args[0][0].instance, firstResult, 'factory:' + serviceName + " event pass right arguments");

        secondResult = Backbone.Injector.factory(serviceName, testParams);
        notEqual(secondResult, firstResult, "New class instance has been returned");
        equal(registerEvent.args[1][0].name, serviceName, 'factory:' + serviceName + " event pass right arguments");
        equal(registerEvent.args[1][0].instance, secondResult, 'factory:' + serviceName + " event pass right arguments");

        ok(registerEvent.calledTwice, "Event factory:" + serviceName + ": called twice");

        isConstructorExists.restore();
    });

    //register method test
    test('register method test', function () {
        var registerEvent = this.spy(),
            registerConstructor = this.spy(Backbone.Injector, '_registerConstructor'),
            registerInstance = this.spy(Backbone.Injector, '_registerInstance'),
            testClass = function () {
                this.test = 'test';
            },
            testObject = {
                test: 'test'
            };

        expect(5);

        Backbone.Injector.listenTo(Backbone.Injector, 'register', registerEvent);
        Backbone.Injector.register('constructor', testClass);
        ok(registerConstructor.calledOnce, 'Constructor registered');
        equal(registerEvent.args[0][0].name, 'constructor', '"register" event pass right arguments');

        Backbone.Injector.register('object', testObject);
        ok(registerInstance.calledOnce, 'Object registered');
        equal(registerEvent.args[1][0].name, 'object', '"register" event pass right arguments');

        ok(registerEvent.calledTwice, '"register" event fired twice');

        registerInstance.restore();
        registerConstructor.restore();
        Backbone.Injector.services.constructor = {};
        delete Backbone.Injector.services.constructor;
        Backbone.Injector.services.object = {};
        delete Backbone.Injector.services.object;
    });

    //dropService method test
    test('dropService method test', function () {
        var registerEvent = this.spy(),
            isServiceExists = this.stub(Backbone.Injector, '_isServiceExists'),
            clearConstructor = this.spy(Backbone.Injector, '_clearConstructor'),
            clearInstance = this.spy(Backbone.Injector, '_clearInstance'),
            name = 'instance';

        expect(4);

        Backbone.Injector.listenTo(Backbone.Injector, 'dropDependency:' + name, registerEvent);

        isServiceExists.returns(false);
        Backbone.Injector.dropService(name);

        ok(!clearConstructor.calledOnce && !clearInstance.calledOnce, "Service doesn't exists");

        isServiceExists.returns(true);
        Backbone.Injector.dropService(name);
        ok(registerEvent.calledOnce, 'dropDependency:' + name + " event has been triggered");
        ok(clearConstructor.calledOnce, "Constructor deleted");
        ok(clearInstance.calledOnce, "Object deleted");

        isServiceExists.restore();
        clearConstructor.restore();
        clearInstance.restore();
    });

    //clearCache method test
    test('clearCache method test', function () {
        var isConstructorExists = this.stub(Backbone.Injector, '_isConstructorExists'),
            isInstanceExists = this.stub(Backbone.Injector, '_isInstanceExists'),
            clearInstance = this.spy(Backbone.Injector, '_clearInstance'),
            name = 'instance';

        isConstructorExists.withArgs(name).returns(true);
        isInstanceExists.withArgs(name).returns(true);

        Backbone.Injector.clearCache(name);

        ok(clearInstance.calledOnce, "Cache is cleared");

        isConstructorExists.restore();
        isInstanceExists.restore();
        clearInstance.restore();
    });
}());

(function () {
    'use strict';

    module('Backbone standard classes mixin test', {
        setup: function () {
            this.classes = {};

            this.classes.model = new Backbone.Model();
            this.classes.collection = new Backbone.Collection();
            this.classes.view = new Backbone.View();
            this.classes.router = new Backbone.Router();
        },

        teardown: function () {
            this.classes = {};
            delete this.classes;
        }
    });

    test('inject method test', function () {
        var inject = this.stub(Backbone.Injector, 'inject');

        this.classes.model.inject('test');
        equal(inject.callCount, 1, 'Backbone.Injector.inject method has been called from Backbone.Model');
        this.classes.collection.inject('test');
        equal(inject.callCount, 2, 'Backbone.Injector.inject method has been called from Backbone.Collection');
        this.classes.view.inject('test');
        equal(inject.callCount, 3, 'Backbone.Injector.inject method has been called from Backbone.View');
        this.classes.router.inject('test');
        equal(inject.callCount, 4, 'Backbone.Injector.inject method has been called from Backbone.Router');

        inject.restore();
    });

    test('factory method test', function () {
        var factory = this.stub(Backbone.Injector, 'factory');

        this.classes.model.factory('test');
        equal(factory.callCount, 1, 'Backbone.Injector.factory method has been called from Backbone.Model');
        this.classes.collection.factory('test');
        equal(factory.callCount, 2, 'Backbone.Injector.factory method has been called from Backbone.Collection');
        this.classes.view.factory('test');
        equal(factory.callCount, 3, 'Backbone.Injector.factory method has been called from Backbone.View');
        this.classes.router.factory('test');
        equal(factory.callCount, 4, 'Backbone.Injector.factory method has been called from Backbone.Router');

        factory.restore();
    });

    test('register method test', function () {
        var register = this.stub(Backbone.Injector, 'register'),
            obj = {
                test: 'test'
            };

        this.classes.model.register('test', obj);
        equal(register.callCount, 1, 'Backbone.Injector.register method has been called from Backbone.Model');
        this.classes.collection.register('test', obj);
        equal(register.callCount, 2, 'Backbone.Injector.register method has been called from Backbone.Collection');
        this.classes.view.register('test', obj);
        equal(register.callCount, 3, 'Backbone.Injector.register method has been called from Backbone.View');
        this.classes.router.register('test', obj);
        equal(register.callCount, 4, 'Backbone.Injector.register method has been called from Backbone.Router');

        register.restore();
    });

    test('dropDependency method test', function () {
        var dropDependency = this.stub(Backbone.Injector, 'dropService');

        this.classes.model.dropDependency('test');
        equal(dropDependency.callCount, 1, 'Backbone.Injector.dropService method has been called from Backbone.Model');
        this.classes.collection.dropDependency('test');
        equal(dropDependency.callCount, 2, 'Backbone.Injector.dropService method has been called from Backbone.Collection');
        this.classes.view.dropDependency('test');
        equal(dropDependency.callCount, 3, 'Backbone.Injector.dropService method has been called from Backbone.View');
        this.classes.router.dropDependency('test');
        equal(dropDependency.callCount, 4, 'Backbone.Injector.dropService method has been called from Backbone.Router');

        dropDependency.restore();
    });

    test('isRegistered method test', function () {
        var isRegistered = this.stub(Backbone.Injector, 'isRegistered');

        this.classes.model.isRegistered('test');
        equal(isRegistered.callCount, 1, 'Backbone.Injector.isRegistered method has been called from Backbone.Model');
        this.classes.collection.isRegistered('test');
        equal(isRegistered.callCount, 2, 'Backbone.Injector.isRegistered method has been called from Backbone.Collection');
        this.classes.view.isRegistered('test');
        equal(isRegistered.callCount, 3, 'Backbone.Injector.isRegistered method has been called from Backbone.View');
        this.classes.router.isRegistered('test');
        equal(isRegistered.callCount, 4, 'Backbone.Injector.isRegistered method has been called from Backbone.Router');

        isRegistered.restore();
    });
}());