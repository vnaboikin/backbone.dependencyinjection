# Backbone Dependency Injection #
-----------------------------
# Introduction #

backbone.dependencyinjection plugin adds Dependency Injection to the Backbone

## Download ##
[Download 0.1.3](https://bitbucket.org/vnaboikin/backbone.dependencyinjection/downloads)

## Usage ##

register service by calling `register` method:

```js

var Collection = Backbone.Collection.extend();

var Model = Backbone.Model.extend({
    defaults: {
        key: 'value'
    }
});

var View = Backbone.View.extend({
    model: 'inject:mymodel', // injecting model
    collection: 'inject:mycollection' // injection collection
});

var Router = Backbone.Router.extend({
    initialize: function () {
        'use strict';
        var myModel = new MyModel();

        this.register('mycollection', MyCollection); //registering service 'mycollection'
        this.register('mymodel', myModel); //registering service 'mymodel'
        this.register('myview', MyView); //registering service 'myview'
    },
    
    routes: {
        '*action': 'defaultAction'
    },
    
    defaultAction: function () {
        this.defaultView = this.inject('myview');
        console.log(this.defaultView.model.get('key')); // 'value'
        console.log(this.defaultView.collection instanceof Collection); // true
    }
});

var router = new Router();

```

### Factory ###


```js
var Collection = Backbone.Collection.extend();
var View = Backbone.View.extend({
    collection: 'inject:mycollection'
});

var Router = Backbone.Router.extend({
    routes: {
        '*action': 'defaultAction'
    },

    initialize: function () {
        'use strict';
        var myModel = new MyModel();

        this.register('mycollection', MyCollection);
        this.register('collectionModel', CollectionModel);
        
        this.listenTo(Backbone.Injector, 'inject:mycollection', function (object) {
            var i = 10;

            while (i > 0) {
                object.service.add(this.factory('collectionModel', {idx: i}));
                i--;
            }
        });
    },

    defaultAction: function () {
        'use strict';
        this.defaultView = this.inject('myview');
        this.defaultView.collection.at(0).get('idx'); // 10
    }
});

router = new Router();
```


## API ##
Backbone.Model, Backbone.Collection, Backbone.View and Backbone.Router are extended by the following methods:

### inject ###

Injects registered service.


```js
this.inject(serviceName, [serviceConstructorParams])
```
or in class constructor:

```js
   ...
   propertyName: 'inject:serviceName'
   ...
```
if `inject` of the Class constructor is called for the first time a new instance of this Class will be created,
next times it returns this instance from the cache.

#### example ####
```js
var View = Backbone.View.extend({
    model: 'inject:mymodel',
    
    initialize: function () {
        this.collection = this.inject('mycollection');
    }
})
```

To use `inject` from constructor with require.js you should load Dependency injection plugin to the module 
in which you are going to use it:

#### example ####
```js
define([
    'backbone',
    'backbone.dependencyinjection'
    ], function (Backbone) {
    'use strict';

    var ChildView;
    
    ChildView = Backbone.View.extend({
        model: 'inject:child-model',
        ....
    });

    return ChildView;  
        
});
```

### register ###


```js
this.register('serviceName', service)
```
service parameter can be either an object instance or a constructor.
If service parameter is a constructor then an instance of the Class will be created
on the first `inject` call

#### example ####

registers Class constructor:

```js
var Router = Backbone.Router.extend({
    initialize: function () {
        this.register('mymodel', MyModel);
    }
});
```

registers object instance:

```js
var Model = Backbone.Model.extend({
    initialize: function () {
        this.register('globalModel', this);
    }
})
```
### factory ###
```js
this.factory('serviceName', optionalParameters);
```
returns a new instance of registered service Class.

#### example ####

```js
...
initialize: function () {
    this.childview0 = this.factory('myview');
    this.childview1 = this.factory('myview');
}
...
```
### dropService ###
```js
this.dropService('serviceName')
```

deletes service completely from repository

#### example ####

```js
close: function () {
    this.dropService('globalModel')
}
```

## Dependencies ##

Backbone > 1.0, underscore.js

## License ##
MIT
